Geoclue: The Geoinformation Service
===================================

Geoclue is a D-Bus geoinformation service. The goal of the Geoclue project is to
make creating location-aware applications as simple as possible.

Geoclue is Free Software, licensed under GNU GPLv2+.

# Troubleshooting and known limitations

  * Geoclue requires glib >= 2.44.0, which is not available on Ubuntu 14.04.
